from django.db import models

class Page(models.Model):

    name = models.CharField(max_length=31)
    homepage = models.URLField()

    url_pattern = models.CharField(max_length=511)
    url_pattern_year = models.CharField(max_length=511, blank=True, null=True)
    url_pattern_city = models.CharField(max_length=511, blank=True, null=True)
    url_pattern_year_city = models.CharField(max_length=511, blank=True, null=True)

    module_find_url = models.CharField(max_length=31)
    module_find_data = models.CharField(max_length=31)
    module_find_photos = models.CharField(max_length=31)

    def __str__(self):
        return "%s" % (self.name, )


class CarType(models.Model):

    name = models.CharField(max_length=31)
    keyword = models.CharField(max_length=31)

    def __str__(self):
        return "%s" % (self.name, )


class PageCarSpecific(models.Model):

    page = models.ForeignKey(Page, on_delete=models.CASCADE, related_name="cars")
    car_type = models.ForeignKey(CarType, on_delete=models.CASCADE, related_name="pages")

    category1 = models.CharField(max_length=31, blank=True, null=True)
    category2 = models.CharField(max_length=31, blank=True, null=True)
    category3 = models.CharField(max_length=31, blank=True, null=True)

    def __str__(self):
        return "%s: %s" % (self.page, self.car_type)