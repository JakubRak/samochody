from django.contrib import admin

from categories.models import Page, CarType, PageCarSpecific

class PageAdmin(admin.ModelAdmin):
    list_display=('name', 'homepage', 'url_pattern')


class CarTypeAdmin(admin.ModelAdmin):
    list_display=('name', 'keyword')


class PageCarSpecificAdmin(admin.ModelAdmin):
    list_display=('page', 'car_type', 'category1', 'category2')


admin.site.register(Page, PageAdmin)
admin.site.register(CarType, CarTypeAdmin)
admin.site.register(PageCarSpecific, PageCarSpecificAdmin)
