from django.core.management.base import BaseCommand

from categories.database.utils import DataBase

class Command(BaseCommand):
    help = 'Saves all categories'

    def handle(self, *args, **options):
        he = DataBase()
        he.run()
        return