from categories.models import Page

class Pages(object):
    
    data = [
        {
            "name":"auto.pl",
            "homepage":"http://www.auto.pl/gielda/osobowe.php",
            "url-pattern":"http://www.auto.pl/gielda/osobowy_showlist.php?z4z_in_1=#category1#&zfn_in_1=#category2#&zyr=#page#",
            "url-pattern-year":"http://www.auto.pl/gielda/osobowy_showlist.php?z4z_in_1=#category1#&zfn_in_1=#category2#&zbb_od=#year#&zbb_do=#year#&zyr=#page#",
            "url-pattern-city":"http://www.auto.pl/gielda/osobowy_showlist.php?z4z_in_1=#category1#&zfn_in_1=#category2#&z24c=31-010&z23x=20&zyr=#page#",
            "url-pattern-year-city":"http://www.auto.pl/gielda/osobowy_showlist.php?z4z_in_1=#category1#&zfn_in_1=#category2#&zbb_od=#year#&zbb_do=#year#&z24c=31-010&z23x=20&zyr=#page#",
            "module-url":"autopl",
            "module-data":"autopl",
            "module-photo":"autopl"
        },
        {
            "name":"otomoto",
            "homepage":"https://www.otomoto.pl/",
            "url-pattern":"https://www.otomoto.pl/osobowe/#category1#/#category2#/?search[new_used]=on&page=#page#",
            "url-pattern-year":"https://www.otomoto.pl/osobowe/#category1#/#category2#/krakow/?search[new_used]=on&page=#page#",
            "url-pattern-city":"https://www.otomoto.pl/osobowe/#category1#/#category2#/od-#year#/?search[new_used]=on&search%5Bfilter_float_year%3Ato%5D=#year#&page=#page#",
            "url-pattern-year-city":"https://www.otomoto.pl/osobowe/#category1#/#category2#/od-#year#/krakow/?search[new_used]=on&search[filter_float_year:to]=#year#&search[dist]=15&page=#page#",
            "module-url":"otomoto",
            "module-data":"otomoto",
            "module-photo":"otomoto"
        },
        {
            "name":"olx",
            "homepage":"https://www.olx.pl/motoryzacja/samochody/",
            "url-pattern":"https://www.olx.pl/motoryzacja/samochody/#category1#/#category2#/?page=#page#",
            "url-pattern-year":"https://www.olx.pl/motoryzacja/samochody/#category1#/#category2#/?search[filter_float_year:from]=#year#&search[filter_float_year:to]=#year#&page=#page#",
            "url-pattern-city":"https://www.olx.pl/motoryzacja/samochody/#category1#/#category2#/krakow/?search[dist]=15&page=#page#",
            "url-pattern-year-city":"https://www.olx.pl/motoryzacja/samochody/#category1#/#category2#/krakow/?search[filter_float_year:from]=#year#&search[filter_float_year:to]=#year#&search[dist]=15&page=#page#",
            "module-url":"olx",
            "module-data":"olx",
            "module-photo":"olx"
        },
        {
            "name":"gratka",
            "homepage":"https://gratka.pl/motoryzacja/osobowe",
            "url-pattern":"https://gratka.pl/motoryzacja/osobowe/#category1#/#category2#/sprzedaz?page=#page#",
            "url-pattern-year":"https://gratka.pl/motoryzacja/osobowe/#category1#/#category2#/krakow/sprzedaz?page=#page#",
            "url-pattern-city":"https://gratka.pl/motoryzacja/osobowe/#category1#/#category2#/sprzedaz?rok-produkcji:min=#year#&rok-produkcji:max=#year#&page=#page#",
            "url-pattern-year-city":"https://gratka.pl/motoryzacja/osobowe/#category1#/#category2#/krakow/sprzedaz?rok-produkcji:min=#year#&rok-produkcji:max=#year#&page=#page#",
            "module-url":"gratka",
            "module-data":"gratka",
            "module-photo":"gratka"
        },
        {
            "name":"sprzedajemy",
            "homepage":"https://sprzedajemy.pl/motoryzacja/samochody-osobowe",
            "url-pattern":"https://sprzedajemy.pl/motoryzacja/samochody-osobowe/#category1#/#category2#?offset=#page#",
            "url-pattern-year":"https://sprzedajemy.pl/motoryzacja/samochody-osobowe/#category1#/#category2#?inp_attribute_466[from]=#year#&inp_attribute_466[to]=#year#&offset=#page#",
            "url-pattern-city":"https://sprzedajemy.pl/motoryzacja/samochody-osobowe/#category1#/#category2#?inp_location_id=36906&offset=#page#",
            "url-pattern-year-city":"https://sprzedajemy.pl/motoryzacja/samochody-osobowe/#category1#/#category2#?inp_location_id=36906&inp_attribute_466[from]=#year#&inp_attribute_466[to]=#year#&offset=#page#",
            "module-url":"sprzedajemy",
            "module-data":"sprzedajemy",
            "module-photo":"sprzedajemy"
        },
        {
            "name":"autotrader",
            "homepage":"https://www.autotrader.pl/osobowe",
            "url-pattern":"https://www.autotrader.pl/szukaj/osobowe/#category1#/#category2##page#",
            "url-pattern-year":"https://www.autotrader.pl/szukaj/osobowe/#category1#/#category2##page#?rok_od=#year#&rok_do=#year#",
            "url-pattern-city":"https://www.autotrader.pl/szukaj/osobowe/#category1#/#category2#/malopolskie/krakow#page#",
            "url-pattern-year-city":"https://www.autotrader.pl/szukaj/osobowe/#category1#/#category2#/malopolskie/krakow#page#?rok_od=#year#&rok_do=#year#",
            "module-url":"autotrader",
            "module-data":"autotrader",
            "module-photo":"autotrader"
        },
        {
            "name":"autogielda",
            "homepage":"https://www.autogielda.pl/sprzedam.html",
            "url-pattern":"https://www.autogielda.pl/sprzedam_#category1#_#category2#.html?strona=#page#",
            "url-pattern-year":"https://www.autogielda.pl/sprzedam_#category1#_#category2#.html?N2=#year#%2C#year#&strona=#page#",
            "url-pattern-city":"https://www.autogielda.pl/sprzedam_#category1#_#category2#.html?miejscowosc=kraków&wojewodztwo=MP&strona=#page#",
            "url-pattern-year-city":"https://www.autogielda.pl/sprzedam_#category1#_#category2#.html?miejscowosc=kraków&wojewodztwo=MP&N2=#year#%2C#year#&strona=#page#",
            "module-url":"autogielda",
            "module-data":"autogielda",
            "module-photo":"autogielda"
        }
    ]

    def save_data(self):
        for record in self.data:
            if record['name'] == '' and record['homepage'] == '':
                continue
            self.save_record(record)
        return

    def save_record(self, data):
        
        pages = Page.objects.all()
        page = Page()
        p = ""
        if data['name'] != '':
            p = pages.filter(name=data['name'])
            if p.count() > 0:
                page = p[0]
        if data['homepage'] != '' and (p == "" or p.count() == 0):
            p = pages.filter(homepage=data['homepage'])
            if p.count() > 0:
                page = p[0]

        page.name = data['name']
        page.homepage = data['homepage']
        
        page.url_pattern = data['url-pattern']
        page.url_pattern_year = data['url-pattern-year']
        page.url_pattern_city = data['url-pattern-city']
        page.url_pattern_year_city = data['url-pattern-year-city']
        
        page.module_find_url = data['module-url']
        page.module_find_data = data['module-data']
        page.module_find_photos = data['module-photo']
        
        page.save()
        
        return