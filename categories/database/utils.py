from categories.database.page import Pages
from categories.database.car import Cars
from categories.database.carpage import CarPages

class DataBase(object):

    objects = [Pages, Cars, CarPages]

    def run(self):
        for el in self.objects:
            obj = el()
            obj.save_data()
        return