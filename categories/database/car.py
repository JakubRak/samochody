from categories.models import CarType

class Cars(object):

    data = [
        {
            "name":"Skoda octavia",
            "keyword":"skoda-octavia"
        },
        {
            "name":"Citroen xsara picasso",
            "keyword":"citroen-xsara-picasso"
        },
        {
            "name":"Citroen C4 picasso",
            "keyword":"citroen-c4-picasso"
        },
        {
            "name":"Citroen C4 picasso grand",
            "keyword":"citroen-c4-picasso-grand"
        },
        {
            "name":"Renault espace",
            "keyword":"renault-espace"
        },
        {
            "name":"Renault scenic",
            "keyword":"renault-scenic"
        },
        {
            "name":"Reanult grand espace",
            "keyword":"renault-espace-grand"
        },
        {
            "name":"Renault grand scenic",
            "keyword":"renault-espace-scenic"
        },
        {
            "name":"VW sharan",
            "keyword":"vw-sharan"
        },
        {
            "name":"VW passat",
            "keyword":"vw-passat"
        },
        {
            "name":"VW golf plus",
            "keyword":"vw-golf-plus"
        },
        {
            "name":"Ford mondeo",
            "keyword":"ford-mondeo"
        },
        {
            "name":"Chrysler town & country",
            "keyword":"chrysler-town-country"
        }
    ]

    def save_data(self):
        for record in self.data:
            if record['name'] == '' and record['keyword'] == '':
                continue
            self.save_record(record)
        return

    def save_record(self, data):
        
        cars = CarType.objects.all()
        car = CarType()
        c = ""
        if data['keyword'] != '':
            c = cars.filter(keyword=data['keyword'])
            if c.count() > 0:
                car = c[0]
        if data['name'] != '' and (c == "" or c.count() == 0):
            c = cars.filter(name=data['name'])
            if c.count() > 0:
                car = c[0]

        car.name = data['name']
        car.keyword = data['keyword']
        
        car.save()
        
        return