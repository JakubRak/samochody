from datetime import datetime
from json import dumps

from django.db.models import Min, Max, Avg
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from categories.models import CarType, Page, PageCarSpecific
from offers.models import Offer
from tools.models import JobCreator, Job

def jsonResponse(dictionary):
    json = dumps(dictionary)
    return HttpResponse(json)

class Ranges(object):

    def get_years_choices(self):
        result = []
        this_year = datetime.now().year
        years = list(range(2000, this_year+1))
        for year in years:
            result.append([year, year])
        return result

    def get_cars(self):
        return CarType.objects.all()

    def get_cars_choices(self):
        result = []
        cars = self.get_cars()
        for car in cars:
            result.append([car.keyword, car.name])
        return result

    def get_pages(self):
        return Page.objects.all()

    def get_pages_choices(self):
        result = []
        pages = self.get_pages()
        for page in pages:
            result.append([page.name, page.name])
        return result

    def add_all_context(self, context):
        context['years'] = self.get_years_choices()
        context['cars'] = self.get_cars_choices()
        context['pages'] = self.get_pages_choices()
        return context


class Home(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class PrepareTask(TemplateView):
    template_name = "prepare.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        r = Ranges()
        context = r.add_all_context(context)
        return context


class AddTaskHandler(object):

    def create_one_job(self, year, car, page, is_city):
        try:
            pcs = PageCarSpecific.objects.get(car_type=car, page=page)
        except Exception as e:
            return 1

        jc = JobCreator()
        jc.car_page = pcs
        jc.year = year
        jc.is_city = is_city

        try:
            jc.save()
        except Exception as e:
            return 1
        return 0

    def create_jobs(self, ranges):
        incorrects = 0
        for year in ranges['year']:
            for car in ranges['car']:
                for page in ranges['page']:
                    for is_city in ranges['is_city']:
                        incorrects += self.create_one_job(
                                year, car, page, is_city)
        if incorrects > 0:
            return {'error': str(incorrects) + " jobs not created"}
        return {}

    def get_ranges(self, data):
        result = {'year': [], 'car': [], 'page': [], 'is_city': []}
        r = Ranges()
        
        if data['year'] == 'all':
            for year in r.get_years_choices():
                result['year'].append(year[0])
            result['year'].append(None)
        else:
            result['year'].append(int(data['year']))

        if data['car'] == 'all':
            for car in r.get_cars():
                result['car'].append(car)
        else:
            try:
                p = CarType.objects.get(keyword=data['car'])
                result['car'].append(p)
            except Exception as e:
                return {'error': str(e)}

        if data['page'] == 'all':
            for page in r.get_pages():
                result['page'].append(page)
        else:
            try:
                p = Page.objects.get(name=data['page'])
                result['page'].append(p)
            except Exception as e:
                return {'error': str(e)}

        result['is_city'].append(True)
        if data['is_city'] == 'all':
            result['is_city'].append(False)

        return result

    def is_query_correct(self, data):
        mandatories = ['year', 'car', 'page', 'is_city']
        available = data.keys()
        for mandatory in mandatories:
            if mandatory not in available:
                return False
        return True

    def process_data(self, raw_data):
        return raw_data.dict()

    def process_request(self, request):
        raw_data = request.POST

        data = self.process_data(raw_data)
        if not self.is_query_correct(data):
            return {'state': 'ERR', 'message': 'incorrect data input'}

        ranges = self.get_ranges(data)
        if 'error' in ranges.keys():
            return {'state': 'ERR', 'message': ranges['error']}

        creation = self.create_jobs(ranges)
        if 'error' in creation.keys():
            return {'state': 'ERR', 'message': creation['error']}
        return {'state': 'OK', 'message': 'OK'}


def AddTask(request):
    if request.method != 'POST':
        result = {'state': "ERROR", 'message': "Wrong request type"}
        return jsonResponse(result)

    handler = AddTaskHandler()
    result = handler.process_request(request)
    return jsonResponse(result)


class ShowForm(TemplateView):
    template_name = "show.html"

    def analyse_car(self, car_name):
        # returns list:
            # [car_name, 
            # offers_count_city, mean_price_city, 
            # offers_count_poland, offers_price_poland]
        car = CarType.objects.get(keyword=car_name)
        result = [car.name, car.keyword]
        offers_basic = Offer.objects.filter(car=car)

        offers_city = offers_basic.filter(is_city=True)
        count = offers_city.count()
        if(count > 0):
            result.append("%d" % count)
            average = offers_city.aggregate(Avg('price'))
            average = average[list(average.keys())[0]]
            result.append("%.0f" % average)
        else:
            result.append("-")

        offers_country = offers_basic.filter(is_city=False)
        count = offers_country.count()
        result.append("%d" % count)
        if(count > 0):
            average = offers_country.aggregate(Avg('price'))
            average = average[list(average.keys())[0]]
            result.append("%.0f" % average)
        else:
            result.append("-")

        return result

    def generate_table(self, cars_list):
        result = []
        for car in cars_list:
            result.append(self.analyse_car(car[0]))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        r = Ranges()
        cars_list = r.get_cars_choices()
        context['cars'] = cars_list
        context['glance'] = self.generate_table(cars_list)
        return context


class ShowDetails(TemplateView):
    template_name = "details.html"

    def year_analyse(self, keyword, year=None):
        year_str = str(year)
        if year == None:
            year = 0
            year_str = "wszystkie"
        result = [year_str, year]

        search = Offer.objects.filter(car__keyword=keyword)
        if year > 0:
            search = search.filter(offer_year=year)
        
        count = search.count()
        result.append(count)
        if count > 0:
            value = search.aggregate(Avg('price'))
            value = value[list(value.keys())[0]]
            if value is None:
                result.append("-")
            else:
                result.append("%.0f" % value)
        else:
            result.append("-")

        search = search.filter(is_city=True)
        count = search.count()
        result.append(count)
        if count > 0:
            value = search.aggregate(Avg('price'))
            value = value[list(value.keys())[0]]
            if value is None:
                result.append("-")
            else:
                result.append("%.0f" % value)
            value = search.aggregate(Min('price'))
            value = value[list(value.keys())[0]]
            if value is None:
                result.append("-")
            else:
                result.append("%d" % value)
            value = search.aggregate(Max('price'))
            value = value[list(value.keys())[0]]
            if value is None:
                result.append("-")
            else:
                result.append("%d" % value)
        else:
            result.append("-")
            result.append("-")
            result.append("-")

        return result

    def all_analysis(self, keyword):
        r = Ranges()
        years = r.get_years_choices()
        result = []
        for year in years:
            result.append(self.year_analyse(keyword, year[0]))
        result.append(self.year_analyse(keyword, None))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        keyword = kwargs['keyword']
        context['keyword'] = keyword
        context['car'] = CarType.objects.get(keyword=keyword)
        context['detailed'] = self.all_analysis(keyword)
        return context


class ShowYearDetails(TemplateView):
    template_name = "year_details.html"

    def get_ranges(self, offers):
        result = [[], []]

        value = offers.aggregate(Min('price'))
        value = value[list(value.keys())[0]]
        if value is None:
            value = 0
        result[0].append(value)
        value = offers.aggregate(Max('price'))
        value = value[list(value.keys())[0]]
        if value is None:
            value = 1
        result[0].append(value)
        value = offers.aggregate(Min('mileage'))
        value = value[list(value.keys())[0]]
        if value is None:
            value = 0
        result[1].append(value)
        value = offers.aggregate(Max('mileage'))
        value = value[list(value.keys())[0]]
        if value is None:
            value = 1
        result[1].append(value)

        rang = result[0][1] - result[0][0]
        if rang == 0:
            rang = 1
        result[0].append(rang)
        rang = result[1][1] - result[1][0]
        if rang == 0:
            rang = 1
        result[1].append(rang)

        return result

    def generate_one_dict(self, offer, ranges):
        dictionary = {}

        mileage = offer.mileage
        price = offer.price
        dictionary['mileage'] = mileage
        dictionary['price'] = price

        point_price = 0
        if price:
            point_price = (ranges[0][1] - price) / ranges[0][2]

        point_mileage = 0
        if mileage:
            point_mileage = (ranges[1][1] - mileage) / ranges[1][2]

        points = point_price*4 + point_mileage
        dictionary['points'] = "%.3f" % points

        dictionary['title'] = offer.title
        dictionary['url'] = offer.url
        dictionary['short_url'] = offer.url[:12] + "..."
        dictionary['pk'] = offer.pk

        return dictionary

    def generate_scoring(self, car, year):

        offers = Offer.objects.filter(car=car)
        offers = offers.filter(is_city=True)
        if int(year) > 0:
            offers = offers.filter(offer_year=year)

        ranges = self.get_ranges(offers)
        result = []
        for offer in offers:
            result.append(self.generate_one_dict(offer, ranges))

        result = sorted(result, key=lambda el: el['points'], reverse=True)

        return [result, ranges]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        keyword = kwargs['keyword']
        year = kwargs['year']
        context['keyword'] = keyword
        context['year'] = year

        try:
            car = CarType.objects.get(keyword=keyword)
            context['car_name'] = car.name
        except Exception as e:
            print(e)
            context['car_name'] = "ERROR OCCURED"
            context['details'] = []
            context['ranges'] = [[0, 1], [0, 1]]
            return context

        result = self.generate_scoring(car, year)
        context['details'] = result[0]
        context['ranges'] = result[1]
        return context