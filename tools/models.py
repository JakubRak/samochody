from django.db import models

class JobCreator(models.Model):

    STATUS_INITIALIZED = 10
    STATUS_PROCESSING = 20
    STATUS_URL_ERROR = 30
    STATUS_PROCESSED = 90
    STATUS_FINISHED_ERROR = 100

    VERBOSE_STATUS = (
        (STATUS_INITIALIZED, "Initialized"),
        (STATUS_PROCESSING, "Job taken"),
        (STATUS_URL_ERROR, "Connection error occured"),
        (STATUS_PROCESSED, "Data extracted, job finished"),
        (STATUS_FINISHED_ERROR, "Unable to finish job")
        )

    car_page = models.ForeignKey('categories.PageCarSpecific', on_delete=models.CASCADE, related_name="major_jobs")

    year = models.CharField(max_length=15, null=True, blank=True)
    is_city = models.BooleanField(default=False)
    status = models.IntegerField(default=STATUS_INITIALIZED, choices=VERBOSE_STATUS)

    def __str__(self):
        return "%s [%s]" % (self.car_page, self.get_status_display())


class Job(models.Model):

    STATUS_INITIALIZED = 10
    STATUS_PROCESSING = 20
    STATUS_URL_ERROR = 30
    STATUS_OPENED = 40
    STATUS_PROCESSED = 90
    STATUS_FINISHED_ERROR = 100

    VERBOSE_STATUS = (
        (STATUS_INITIALIZED, "Initialized"),
        (STATUS_PROCESSING, "Job taken"),
        (STATUS_URL_ERROR, "Connection error occured"),
        (STATUS_OPENED, "URL opened"),
        (STATUS_PROCESSED, "Data extracted, job finished"),
        (STATUS_FINISHED_ERROR, "Unable to finish job")
        )

    url = models.URLField()
    status = models.IntegerField(default=STATUS_INITIALIZED, choices=VERBOSE_STATUS)
    page_data = models.TextField(null=True, blank=True)
    main_job = models.ForeignKey(JobCreator, on_delete=models.CASCADE, related_name="subjobs")

    time_initialized = models.DateTimeField(auto_now_add=True)
    time_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s [%s]" % (self.url, self.get_status_display())


class ConnectionFailure(models.Model):

    error_code = models.CharField(max_length=15, null=True, blank=True)
    error_message = models.CharField(max_length=127, null=True, blank=True)
    error_domain = models.CharField(max_length=63, null=True, blank=True)
    error_page = models.TextField(null=True, blank=True)
    first_spotted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        if self.error_message:
            return self.error_message
        if self.error_code:
            return self.error_code
        if self.error_page and len(self.error_page) > 32:
            return self.error_page[:29] + "..."
        return self.error_page


class JobConnectionFailure(models.Model):

    error = models.ForeignKey(ConnectionFailure, on_delete=models.CASCADE, related_name="jobs")
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name="offers")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.job)

