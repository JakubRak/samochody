# Generated by Django 2.1.5 on 2019-02-03 18:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('categories', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConnectionFailure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('error_code', models.CharField(blank=True, max_length=15, null=True)),
                ('error_message', models.CharField(blank=True, max_length=127, null=True)),
                ('error_domain', models.CharField(blank=True, max_length=63, null=True)),
                ('error_page', models.TextField(blank=True, null=True)),
                ('first_spotted', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
                ('status', models.IntegerField(choices=[(10, 'Initialized'), (20, 'Job taken'), (30, 'Connection error occured'), (40, 'URL opened'), (90, 'Data extracted, job finished'), (100, 'Unable to finish job')], default=10)),
                ('page_data', models.TextField(blank=True, null=True)),
                ('time_initialized', models.DateTimeField(auto_now_add=True)),
                ('time_updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='JobConnectionFailure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('error', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jobs', to='tools.ConnectionFailure')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offers', to='tools.Job')),
            ],
        ),
        migrations.CreateModel(
            name='JobCreator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.CharField(blank=True, max_length=15, null=True)),
                ('is_city', models.BooleanField(default=False)),
                ('status', models.IntegerField(choices=[(10, 'Initialized'), (20, 'Job taken'), (30, 'Connection error occured'), (90, 'Data extracted, job finished'), (100, 'Unable to finish job')], default=10)),
                ('car_page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='major_jobs', to='categories.PageCarSpecific')),
            ],
        ),
        migrations.AddField(
            model_name='job',
            name='main_job',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subjobs', to='tools.JobCreator'),
        ),
    ]
