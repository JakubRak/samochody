from django.core.management.base import BaseCommand

from tools.etl.logic_creator import LogicWorker

class Command(BaseCommand):
    help = 'Process one job creator'

    def handle(self, *args, **options):
        lw = LogicWorker()
        job = lw.get_one_job()
        if job is False:
            return
        lw.run_one_job(job)
        return