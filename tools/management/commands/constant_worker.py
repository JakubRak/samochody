from time import sleep

from django.core.management.base import BaseCommand

from tools.etl.logic_creator import LogicWorker
from tools.etl.logic_runner import JobWorker

class Command(BaseCommand):
    help = 'Process one job creator'

    def job_creator_logic(self):
        lw = LogicWorker()
        job = lw.get_one_job()
        if job is False:
            return False
        lw.run_one_job(job)
        return True

    def job_logic(self):
        jw = JobWorker()
        job = jw.get_one_job()
        if job is False:
            return False
        jw.run_one_job(job)
        return True

    def job_logic_errors(self):
        jw = JobWorker()
        job = jw.get_one_error_job()
        if job is False:
            return False
        jw.run_one_job(job)
        return True

    def handle(self, *args, **options):
        while True:
            sleep(1.5)
            print(" = = = = = STEP = = = = = =")

            result = self.job_creator_logic()
            if result is not False:
                continue

            result = self.job_logic()
            if result is not False:
                continue

            result = self.job_logic_errors()
            if result is False:
                print(" [ [ [ [ [ [ waiting for a job ] ] ] ] ] ] ] ")
                sleep(10)

        return