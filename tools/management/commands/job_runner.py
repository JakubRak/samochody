from django.core.management.base import BaseCommand

from tools.etl.logic_runner import JobWorker

class Command(BaseCommand):
    help = 'Process one job creator'

    def handle(self, *args, **options):
        jw = JobWorker()
        job = jw.get_one_job()
        if job is False:
            return
        jw.run_one_job(job)
        return