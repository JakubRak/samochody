from re import search # compile as re_compile

from bs4 import BeautifulSoup

from tools.etl.connection import Connection

class Extract(object):

    def save_data_to_object(self, string_result, job):
        job.page_data = string_result
        job.save()
        return

    def extract(self, job):
        connect = Connection()
        print("=> URL", job.url)
        data = connect.make_connection(job.url)
        if data[0] is False:
            return False
        self.save_data_to_object(data[1], job)
        return True


class CreatorExtract(object):

    patterns = {}
    job_pattern = {}
    regex = ""

    def __init__(self):
        self.regex = ""
        self.patterns = {
            'autopl': {'key': 'zyr', 'pagination': 'offset', 'multiplier': 20},
            'otomoto': {'key': 'page', 'pagination': 'page'},
            'olx': {'key': 'page', 'pagination': 'page'},
            'gratka': {'key': 'page', 'pagination': 'page'},
            'sprzedajemy': {'key': 'offset', 'pagination': 'offset', 'multiplier': 30, 'cut_begin': 22},
            'autotrader': {'key': '/str-','pagination': 'page', 'pattern': '/str-#page#', 'start': 2, 'cut_begin': 25},
            'autogielda': {'key': 'strona', 'pagination': 'page', 'cut_begin': 26}
            }
        self.job_pattern = {}
        return

    def select_job_pattern(self, job_creator):
        page_data = job_creator.car_page.page
        find_url_key = page_data.module_find_url
        if find_url_key not in self.patterns.keys():
            return False
        self.job_pattern = self.patterns[find_url_key]
        return True

    def pagination_filter(self, tag):
        if tag.name != 'a':
            return False
        if not tag.has_attr('href'):
            return False
        if not tag['href'] or tag['href'] == "#":
            return False
        href = tag['href']
        if self.regex == "":
            return True
        if search(self.regex, href):
            return True
        return False

    def define_regex(self, job_creator):
        job_pattern = self.job_pattern
        keys = list(job_pattern.keys())

        # this string will be our regex
        regex = self.create_url(job_creator, "#PAGE#")

        # cut off all GETs
        position = regex.find("?")
        if position > -1:
            regex = regex[:position]

        # cut beginning letters if necessary
        if 'cut_begin' in keys:
            regex = regex[job_pattern['cut_begin']:]

        # change some characters to be safe for regex
        characters = "\\/[](){}?!<=>|.,*+&-$^"
        additional_pattern = ""
        if 'pattern' in keys:
            additional_pattern = job_pattern['key']
        for character in characters:
            regex = regex.replace(character, "\\"+character)
            additional_pattern = additional_pattern.replace(character, "\\"+character)


        position = regex.find("#PAGE#")
        if position > -1:
            # page data not in GETs
            to_replace = ""
            if regex.find(additional_pattern) > -1:
                # there is pattern included -> replace only "#PAGE#"
                to_replace = "(\d+)"
            else:
                # there is no pattern included -> replace pattern with #PAGE#
                to_replace = job_pattern['pattern']
                to_replace = to_replace.replace("#page#", "(\d+)")
            regex = regex.replace("#PAGE#", to_replace)
            regex += "\??.*"
        else:
            # page data in GETs
            regex += "\?.*&?" + job_pattern['key'] + "\=(\d+)&?.*"

        # save the result
        self.regex = regex
        return True

    def find_max_pagination(self, job_creator, starting_page):
        if self.regex == "":
            result = self.define_regex(job_creator)
            if result is False:
                return 1

        soup = BeautifulSoup(starting_page, features="html.parser")
        tags = soup.find_all(self.pagination_filter)

        result = 1
        for tag in tags:
            partial = search(self.regex, tag.attrs['href'])
            if not partial:
                continue
            partial = int(partial[1])
            if partial > result:
                result = partial

        if result == 1:
            return result

        job_pattern = self.job_pattern
        keys = list(job_pattern.keys())
        if 'multiplier' in keys:
            result = result // job_pattern['multiplier']
        if job_pattern['pagination'] == 'offset':
            result += 1

        return result

    def replace_page_in_url(self, url, page_no):
        if type(page_no) is str:
            return url.replace("#page#", page_no)

        job_pattern = self.job_pattern
        keys = list(job_pattern.keys())

        if 'start' in keys:
            if page_no < job_pattern['start']:
                return url.replace("#page#", "")

        value = page_no
        if job_pattern['pagination'] == 'offset':
            value -= 1
        if 'multiplier' in keys:
            value *= job_pattern['multiplier']

        if 'pattern' in keys:
            pattern = job_pattern['pattern']
            pattern = pattern.replace("#page", str(value))
            return url.replace("#page#", pattern)

        return url.replace("#page#", str(value))

    def create_url(self, job_creator, page_no=1):
        car_page = job_creator.car_page
        page = car_page.page

        url = ""
        if job_creator.is_city:
            if job_creator.year:
                url = page.url_pattern_year_city
            else:
                url = page.url_pattern_city
        else:
            if job_creator.year:
                url = page.url_pattern_year
            else:
                url = page.url_pattern

        if job_creator.year:
            url = url.replace("#year#", str(job_creator.year))
        if car_page.category1:
            url = url.replace("#category1#", car_page.category1)
        if car_page.category2:
            url = url.replace("#category2#", car_page.category2)
        if car_page.category3:
            url = url.replace("#category3#", car_page.category3)
        url = self.replace_page_in_url(url, page_no)

        return url

    def extract(self, job_creator):
        if not self.select_job_pattern(job_creator):
            print("Job pattern not found. Extracting stopped.")
            return False

        connect = Connection()

        starting_url = self.create_url(job_creator)
        print("-> URL", starting_url)
        starting_page = connect.make_connection(starting_url)
        if starting_page[0] is False:
            print("-> ERROR")
            return False
        starting_page = starting_page[1]
        result = [starting_page]

        max_pagination = self.find_max_pagination(job_creator, starting_page)
        for itr in range(2, max_pagination+1):
            url = self.create_url(job_creator, page_no=itr)
            html = connect.make_connection(url)
            if html[0] is False:
                continue
            result.append(html[1])

        return result