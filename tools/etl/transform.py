# -*- coding: UTF-8 -*-

import locale

from datetime import datetime
from re import search, sub

from bs4 import BeautifulSoup

locale.setlocale(locale.LC_TIME, "pl_PL.utf-8")


class Commons(object):

    temp_pattern = {}
    patterns = {}
    # example pattern:
        # {'title': {
        #     'tag': 'div',
        #     'mandatories': ['id'],
        #     'values': [['class', 'title']],
        #     'target': 'text'
        #     }
        # }

    def __init__(self):
        self.patterns = {}
        self.temp_pattern = {}
        return

    def soup_data(self, input_html):
        return BeautifulSoup(input_html, features="html.parser")

    def link_filter(self, tag):
        if tag.name != 'a':
            return False
        if not tag.has_attr('href'):
            return False
        if not tag['href'] or tag['href'] == "#":
            return False
        return True

    def find_all_links(self, input_html, regex=None):
        soup = self.soup_data(input_html)
        links = soup.find_all(self.link_filter)
        if not regex:
            return links
        result = []
        for link in links:
            if search(regex, link.attrs['href']):
                result.append(link)
        return result

    def pattern_filter(self, tag):
        pattern = self.temp_pattern
        keys = list(pattern.keys())
        if pattern == {}:
            return False
        
        if 'tag' in keys:
            if tag.name != pattern['tag']:
                return False
        if 'mandatories' in keys:
            for mandatory in pattern['mandatories']:
                if not tag.has_attr(mandatory):
                    return False
        if 'values' in keys:
            for value in pattern['values']:
                if not tag.has_attr(value[0]):
                    return False
                if type(value[1]) is list:
                    for val in value[1]:
                        if not val in tag[value[0]]:
                            return False
                else:
                    if not value[1] in tag[value[0]]:
                        return False

        if 'text-search' in keys:
            if not search(pattern['text-search'], tag.text):
                return False

        return True

    def find_data_by_pattern(self, soup):
        pattern = self.temp_pattern

        data = soup.find_all(self.pattern_filter)
        if len(data) == 0:
            return None
        tag = data[0]

        target = pattern['target']
        result = ""
        if target == 'text':
            result = tag.text
        elif target == 'attribute':
            result = tag[self.temp_pattern['target-value']]
        else:
            print("Warning: Unknown target")
            return None

        if 'transform-cut-begin' in pattern.keys():
            result = result[pattern['transform-cut-begin']:]
        if 'transform-cut-end' in pattern.keys():
            result = result[:len(result)-pattern['transform-cut-begin']]

        if 'transform' in pattern.keys():
            if pattern['transform'] == 'strip':
                result = result.strip()
            if pattern['transform'] == 'date':
                date_pattern = pattern['transform-pattern']
                try:
                    date_obj = datetime.strptime(result, date_pattern)
                    result = date_obj.date()
                except Exception as e:
                    print(e)
                    return None
            if pattern['transform'] == 'cut_distance':
                mileages = ["km"]
                for mileage in mileages:
                    if result.find(mileage) > -1:
                        result = result[:result.find(mileage)]
                result = sub(r"\s", "", result)
                result = result.replace("tys.", "000")
                try:
                    result = int(result)
                except Exception as e:
                    print(e)
                    return None
            if pattern['transform'] == 'cut_price':
                currencies = ["PLN", "zł", "zÅ"]
                for currency in currencies:
                    if result.find(currency) > -1:
                        result = result[:result.find(currency)]
                result = sub(r"\s", "", result)
                try:
                    result = int(result)
                except Exception as e:
                    print(e)
                    return None


        return result

    def find_data_dict(self, input_html):
        soup = self.soup_data(input_html)
        result = {}
        for key in self.patterns.keys():
            self.temp_pattern = self.patterns[key]
            partial = self.find_data_by_pattern(soup)
            if partial is not None:
                result[key] = partial
        print("=> DICT", list(result.keys()))
        return result


class Transform(object):

    patterns = {
        'autopl': {
            'title': {
                'tag': 'span',
                'values': [['class', 'auto-item-naglowek-title']],
                'target': 'text',
                'transform': 'strip'
            },
            'subtitle': {
                'tag': 'span',
                'values': [['class', 'auto-item-naglowek-sub']],
                'target': 'text',
                'transform': 'strip'
            },
            'description': {
                'tag': 'div',
                'values': [['class', ['col-all-max', 'text-left']]],
                'target': 'text',
                'text-search': '^\s*Opis\s*.*'
            },
            'price': {
                'tag': 'b',
                'target': 'text',
                'text-search': '[0-9\s]+zł',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'li',
                'values': [['class', ['col-md-8', 'col-sm-8']]],
                'target': 'text',
                'text-search': 'przebieg:\s+[0-9\s]+\s*',
                'transform': 'cut_distance',
                'transform-cut-begin': 9
            }
        },
        'otomoto': {
            'title': {
                'tag': 'a',
                'mandatories': ['data-nr', 'href', 'alt'],
                'values': [['class', 'thumb']],
                'target': 'attribute',
                'target-value': 'alt'
            },
            'subtitle': {
                'tag': 'title',
                'target': 'text'
            },
            'description': {
                'tag': 'div',
                'values': [['class', 'offer-description'], ['id', 'description']],
                'target': 'text'
            },
            'offer_date': {
                'tag': 'span',
                'values': [['class', 'offer-meta__value']],
                'target': 'text',
                'transform': 'date',
                'transform-pattern': '%H:%M, %d %B %Y'
            },
            'price': {
                'tag': 'span',
                'values': [['class', 'offer-price__number']],
                'target': 'text',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'span',
                'values': [['class', 'offer-main-params__item']],
                'text-search': '[0-9 ]+ km.*',
                'target': 'text',
                'transform': 'cut_distance'
            }
        },
        'olx': {
            'title': {
                'tag': 'h1',
                'target': 'text',
                'transform': 'strip'
            },
            'description': {
                'tag': 'div',
                'mandatories': ['class'],
                'values': [['id', 'textContent']],
                'target': 'text'
            },
            # 'offer_date': {
            #     'tag': 'em',
            #     'target': 'text'
            # },
            'price': {
                'tag': 'div',
                'values': [['class', 'price-label']],
                'target': 'text',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'strong',
                'target': 'text',
                'text-search': '[0-9 ]+km',
                'transform': 'cut_distance'
            }
        },
        'gratka': {
            'title': {
                'tag': 'h1',
                'mandatories': ['class'],
                'values': [['class', 'sticker__title']],
                'target': 'text'
            },
            'description': {
                'tag': 'div',
                'mandatories': ['id'],
                'values': [['class', 'description']],
                'target': 'text'
            },
            'price': {
                'tag': 'p',
                'values': [['class', 'creditCalc__price']],
                'target': 'text',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'b',
                'values': [['class', 'parameters__value']],
                'text-search': '^\s*[0-9]+\s*$',
                'target': 'text',
                'transform': 'cut_distance'
            }
        },
        'sprzedajemy': {
            'title': {
                'tag': 'span',
                'values': [['class', 'isUrgentTitle']],
                'target': 'text'
            },
            'description': {
                'tag': 'div',
                'values': [['class', 'offerDescription']],
                'target': 'text'
            },
            # 'offer_date': {},
            'price': {
                'tag': 'span',
                'values': [['itemprop', 'price']],
                'target': 'text',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'strong',
                'target': 'text',
                'text-search': '[0-9 ]+km',
                'transform': 'cut_distance'
            }
        },
        'autotrader': {
            'title': {
                'tag': 'img',
                'mandatories': ['src', 'alt', 'title'],
                'target': 'attribute',
                'target-value': 'alt'
            },
            'description': {
                'tag': 'div',
                'values': [['data-navbar-target', 'description']],
                'target': 'text'
            },
            'price': {
                'tag': 'h3',
                'values': [['class', 'basic-info-heading__price']],
                'target': 'text',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'span',
                'target': 'text',
                'text-search': '[0-9 ]km',
                'transform': 'cut_distance'
            }
        },
        'autogielda': {
            'title': {
                'tag': 'h2',
                'target': 'text'
            },
            'description': {
                'tag': 'p',
                'values': [['class', 'more-text']],
                'target': 'text'
            },
            'price': {
                'tag': 'div',
                'values': [['class', 'auto-price']],
                'target': 'text',
                'transform': 'cut_price'
            },
            'mileage': {
                'tag': 'strong',
                'target': 'text',
                'text-search': '\d+ tys\. km',
                'transform': 'cut_distance'
            }
        }
    }

    def transform(self, job):
        # main related objects
        main_job = job.main_job
        car_page = main_job.car_page
        input_html = job.page_data

        # getting proper data from input_html
        class_keyword = car_page.page.module_find_data
        pattern = self.patterns[class_keyword]
        obj = Commons()
        obj.patterns = pattern
        dict_data = obj.find_data_dict(input_html)

        # main data about offer
        url = job.url
        car = car_page.car_type
        dict_data['url'] = url
        dict_data['car'] = car

        # additional data taken from job
        is_year = False
        offer_year = None
        if main_job.year:
            is_year = True
            offer_year = int(main_job.year)
        is_city = main_job.is_city

        if offer_year:
            dict_data['offer_year'] = offer_year
        dict_data['is_year'] = is_year
        dict_data['is_city'] = is_city

        return dict_data


class CreatorTransform(object):

    filters = {
        'autopl': 'http://www\.auto\.pl/gielda/osobowy_showone\.php\?.*&?kod=\w+',
        'otomoto': 'https://www\.otomoto\.pl/oferta/.*-\w+\.html[#]?.*',
        'olx': 'https://www\.olx\.pl/oferta/.*-\w+\.html',
        'gratka': 'https://gratka\.pl/motoryzacja/.*/ob/\d+',
        'sprzedajemy': 'https://.*\.sprzedajemy\.pl/.*-\w+-nr\d+',
        'autotrader': '/oferta/.*-\d+',
        'autogielda': 'https://www\.autogielda\.pl/sprzedam_\w+_\w+,.*,[A-Z0-9]+.html'
        }

    prefixes = {
        'autotrader': 'https://www.autotrader.pl'
    }

    def simplify_url(self, url):
        new_url = url
        found = new_url.find("#")
        if found > -1:
            new_url = new_url[:found]
        return new_url

    def transform(self, job, extracted_list):
        module = job.car_page.page.module_find_url
        filtr = None
        if module in self.filters.keys():
            filtr = self.filters[module]

        result = []
        handler = Commons()
        for data in extracted_list:
            all_links = handler.find_all_links(data, filtr)
            for link in all_links:
                href = link.attrs['href']
                href = self.simplify_url(href)
                if module in self.prefixes.keys():
                    href = self.prefixes[module] + href
                if href not in result:
                    result.append(href)

        return result
