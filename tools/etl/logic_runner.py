from tools.models import Job
from tools.etl.extract import Extract
from tools.etl.transform import Transform
from tools.etl.load import Load

class JobWorker(object):

    def run_one_job(self, job):
        job.status = job.STATUS_PROCESSING
        job.save()

        print("Extracting offer ...")
        handler = Extract()
        extracted = handler.extract(job)
        if extracted is False:
            job.status = job.STATUS_URL_ERROR
            job.save()
            print("URL error during extract occured")
            return False
        print("Offer extracted")

        job.status = job.STATUS_OPENED
        job.save()

        print("Transforming offer ...")
        handler = Transform()
        transformed = handler.transform(job)
        print("Offer transformed")

        print("Loading into database ...")
        handler = Load()
        loaded = handler.load(transformed)
        print("Offer loaded")

        job.status = job.STATUS_PROCESSED
        job.save()
        return True

    def initialized_jobs_count(self):
        jc = Job.objects.filter(status=Job.STATUS_INITIALIZED)
        return jc.count()

    def get_one_job(self):
        count = self.initialized_jobs_count()
        print("Found %d job(s) to start." % count)
        if count == 0:
            print("Nothing to start.")
            return False
        print("One job selected to start.")
        jc = Job.objects.filter(status=Job.STATUS_INITIALIZED)
        jc = jc.order_by("pk")
        return jc[0]

    def get_one_error_job(self):
        error_jobs = Job.objects.filter(status=Job.STATUS_URL_ERROR)
        count = error_jobs.count()
        print("Found %d error job(s) to start." % count)
        if count == 0:
            print("Nothing to start.")
            return False
        print("One job selected to start.")
        jc = error_jobs.order_by("time_updated")
        return jc[0]
