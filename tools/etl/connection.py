from urllib.request import urlopen, Request
from re import compile as re_compile

from tools.models import ConnectionFailure, JobConnectionFailure, Job

class Connection(object):

    def domain_finder(self, url):
        regex_string = "^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)"
        regex = re_compile(regex_string)
        match = regex.match(url)
        if match:
            return match[1]
        return ""

    def push_to_errors_storage(self, error, job=None):
        try:
            url = error.url
            code = error.code
            message = error.fp.msg
            body = error.fp.fp.read().decode("utf-8")
            domain = self.domain_finder(url)
        except Exception as e:
            print(e)
            message = "Unknown error: " + str(e)
            code = 0
            body = ""
            url=None
            domain = ""
        error_object = ConnectionFailure.objects.get_or_create(
                error_code=code, error_message=message, error_domain=domain)
        error_object = error_object[0]
        if not error_object.error_page:
            error_object.error_page = body
            error_object.save()

        new_job = job
        if url and not new_job:
            jobs = Job.objects.filter(url=url)
            if jobs.count() > 0:
                new_job = jobs[0]

        if new_job:
            error_storage = JobConnectionFailure()
            error_storage.error = error_object
            error_storage.job = new_job
            error_storage.save()

        return body

    def find_charset_in_binary(self, input_string):
        regex = re_compile(rb'<meta.*charset=(.*)"')
        result = regex.search(input_string)
        if not result:
            return "utf-8"
        result = result[1]
        result = result.decode("utf-8")
        if result[0] == '"':
            result = result[1:]
        return result

    def make_connection(self, url):
        try:
            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0"
                }
            request_object = Request(url, headers=headers)
            request = urlopen(request_object)
            response = request.read()
            charset = self.find_charset_in_binary(response)
            string = response.decode(charset)
        except Exception as e:
            commons = ['utf-8', 'latin-1']
            for char in commons:
                try:
                    string = response.decode(char)
                    return [True, string]
                except Exception as f:
                    pass
            return [False, self.push_to_errors_storage(e)]
        return [True, string]
        