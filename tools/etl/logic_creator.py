from tools.models import JobCreator
from tools.etl.extract import CreatorExtract
from tools.etl.transform import CreatorTransform
from tools.etl.load import CreatorLoad

class LogicWorker(object):

    def run_one_job(self, job):
        job.status = job.STATUS_PROCESSING
        job.save()

        print("Extracting ...")
        handler = CreatorExtract()
        extracted = handler.extract(job)
        if extracted is False:
            job.status = job.STATUS_FINISHED_ERROR
            job.save()
            print("URL error during extract occured")
            return False
        print("Extracted %d pages" % len(extracted))

        print("Transforming ...")
        handler = CreatorTransform()
        transformed = handler.transform(job, extracted)
        print("Transformed into %d links" % len(transformed))
        print("Loading into database ...")
        handler = CreatorLoad()
        loaded = handler.load(job, transformed)
        print("Loaded %d new jobs" % loaded)

        job.status = job.STATUS_PROCESSED
        job.save()
        return True

    def initialized_jobs_count(self):
        jc = JobCreator.objects.filter(status=JobCreator.STATUS_INITIALIZED)
        return jc.count()

    def get_one_job(self):
        count = self.initialized_jobs_count()
        print("Found %d job(s) to start." % count)
        if count == 0:
            print("Nothing to start.")
            return False
        print("One job selected to start.")
        jc = JobCreator.objects.filter(status=JobCreator.STATUS_INITIALIZED)
        jc = jc.order_by("pk")
        return jc[0]
