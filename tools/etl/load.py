from datetime import date

from offers.models import Offer, PhotoOffer
from categories.models import CarType
from tools.models import Job

class Load(object):

    def check_value(self, value, properties):
        result = True

        types = {'car': CarType,
            'string': str,
            'date': date,
            'integer': int,
            'boolean': bool}
        if type(value) is not types[properties['type']]:
            return False

        if properties['type'] == 'string':
            if 'max_length' in properties.keys():
                if len(value) > properties['max_length']:
                    return False

        if properties['type'] == 'integer':
            if 'min_value' in properties.keys():
                if value < properties['min_value']:
                    return False
            if 'max_value' in properties.keys():
                if value > properties['max_value']:
                    return False

        return True

    def load_offer(self, dict_data):
        mandatories = ['car', 'url']
        all_properties = {
            'title': {'type': 'string', 'max_length': 255},
            'subtitle': {'type': 'string', 'max_length': 255},
            'description': {'type': 'string'},
            'offer_date': {'type': 'date'},
            'price': {'type': 'integer', 'min_value': 0},
            'mileage': {'type': 'integer', 'min_value': 0},
            'offer_year': {'type': 'integer', 'min_value': 1900, 'max_value': 2050},
            'is_year': {'type': 'boolean'},
            'is_city': {'type': 'boolean'}
        }

        all_keys = list(dict_data.keys())
        for key in mandatories:
            if key not in all_keys:
                return False

        offer = Offer.objects.get_or_create(car=dict_data['car'], url=dict_data['url'])
        offer = offer[0]
        for key in all_properties.keys():
            if key not in dict_data.keys():
                continue
            if self.check_value(dict_data[key], all_properties[key]):
                value = dict_data[key]
                offer.__setattr__(key, value)
            else:
                pass

        offer.save()
        return offer

    def load_one_photo(self, photo_data, offer):
        if 'url' not in photo_data.keys()\
                or photo_data['url'] == '':
            return False
        photo = PhotoOffer.objects.get_or_create(car=offer, url=photo_data['url'])

        if 'title' in photo_data.keys():
            photo.title = photo_data['title']

        photo.save()
        return photo

    def load_photos(self, photos_list, offer):
        for photo_data in photos_list:
            self.load_one_photo(photo_data, offer)
        return

    def load(self, dict_data):
        # dict_data is a dictionary with keys:
            # CarType: car
            # string: url, title, subtitle, description
            # integer: price, mileage, offer_year
            # boolean: is_year, is_city
            # date: offer_date
            # list: photos (string: url, title)

        offer = self.load_offer(dict_data)
        if offer is False:
            return False
        if 'photos' in dict_data.keys():
            self.load_photos(dict_data['photos'], offer)
        return True


class CreatorLoad(object):

    def create_job(self, job, url):
        j = Job()
        j.main_job = job
        j.url = url
        j.save()
        return

    def count_jobs(self, url):
        return Job.objects.filter(url=url).count()

    def load(self, job, urls):
        count = 0

        for url in urls:
            if self.count_jobs(url) == 0:
                self.create_job(job, url)
                count += 1

        return count

        