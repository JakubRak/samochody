from django.contrib import admin

from tools.models import JobCreator, Job, ConnectionFailure, JobConnectionFailure

class JobCreatorAdmin(admin.ModelAdmin):
    list_display=('car_page', 'status', 'year', 'is_city')


class JobAdmin(admin.ModelAdmin):
    list_display=('url', 'status', 'main_job', 'time_initialized', 'time_updated')


class ConnectionFailureAdmin(admin.ModelAdmin):
    list_display=('error_domain', 'error_code', 'error_message', 'first_spotted')


class JobConnectionFailureAdmin(admin.ModelAdmin):
    list_display=('date', 'error', 'job')


admin.site.register(JobCreator, JobCreatorAdmin)
admin.site.register(Job, JobAdmin)
admin.site.register(ConnectionFailure, ConnectionFailureAdmin)
admin.site.register(JobConnectionFailure, JobConnectionFailureAdmin)
