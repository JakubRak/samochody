from django.contrib import admin

from offers.models import Offer, PhotoOffer

class OfferAdmin(admin.ModelAdmin):
    list_display=('title', 'subtitle', 'car', 'url', 'price')


class PhotoOfferAdmin(admin.ModelAdmin):
    list_display=('car', 'title', 'url')


admin.site.register(Offer, OfferAdmin)
admin.site.register(PhotoOffer, PhotoOfferAdmin)
