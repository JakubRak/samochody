# Generated by Django 2.1.5 on 2019-02-09 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('offers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='subtitle',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='offer',
            name='title',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
