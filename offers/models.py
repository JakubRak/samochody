from django.db import models

class Offer(models.Model):

    car = models.ForeignKey('categories.CarType', on_delete=models.CASCADE, related_name="offers")
    url = models.URLField()

    title = models.CharField(max_length=255, null=True, blank=True)
    subtitle = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    offer_date = models.DateField(null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    mileage = models.IntegerField(null=True, blank=True) # przebieg samochodu

    offer_year = models.IntegerField(null=True, blank=True)
    is_year = models.BooleanField(default=False)
    is_city = models.BooleanField(default=False)

    def __str__(self):
        if self.title:
            return "%s" % (self.title, )
        if self.subtitle:
            return "%s" % (self.subtitle, )
        return str(self.car)


class PhotoOffer(models.Model):

    car = models.ForeignKey(Offer, on_delete=models.CASCADE, related_name="photos")
    url = models.URLField()
    title = models.CharField(max_length=63, null=True, blank=True)

    def __str__(self):
        return self.get_title()

    def get_title(self):
        if self.title:
            return self.title
        return str(self.url)