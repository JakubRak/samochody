"""samochody URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from tools.views import Home, PrepareTask, AddTask, ShowForm, ShowDetails, ShowYearDetails

urlpatterns = [
    path('show_form/', ShowForm.as_view(), name='show-form'),
    path('show_details/<slug:keyword>/<int:year>/', ShowYearDetails.as_view(), name='show-year-details'),
    path('show_details/<slug:keyword>/', ShowDetails.as_view(), name='show-details'),
    path('add_task/', AddTask, name='add'),
    path('prepare_task/', PrepareTask.as_view(), name='prepare'),
    path('admin/', admin.site.urls),
    path('', Home.as_view(), name='home'),
]
